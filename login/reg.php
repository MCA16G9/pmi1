<html lang="en">
<head>
	<?php 	include("fb_files/fb_index_file/fb_SignUp_file/SignUp.php");
 ?>
	<title>Registration</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">


<!--===============================================================================================-->
<script type="text/javascript" src="fb_files/fb_index_file/fb_js_file/Registration_validation.js"> </script>
</head>

<script>
	function time_get()
	{
		d = new Date();
		mon = d.getMonth()+1;
		time = d.getDate()+"-"+mon+"-"+d.getFullYear()+" "+d.getHours()+":"+d.getMinutes();
		Reg.fb_join_time.value=time;
	}
</script>
<body>
	<form  method="post" onSubmit="return check();" name="Reg">

	<div class="limiter">
		<div class="container-login100">
			<div style="position:fixed;left:8%;top:8%;z-index:2;"> <a href="../index.php" style="color:#000000; text-decoration:none;" onMouseOver="on_head_fb_text()" onMouseOut="out_head_fb_text()"><h3> <b>PMI</b></h3></font> </a> </div>

			<div class="wrap-login101">

				<form class="login100-form validate-form">
					<span class="login100-form-title">

						Registration
					</span>


					<div class="wrap-input100 validate-input" required="required" data-validate = "Valid email is required: ex@abc.xyz">
						<span class >First name</span>
						<input class="input101" type="text" name="first_name" required="required"/>
					</div>
					<div class="wrap-input100 validate-input" required="required" data-validate = "Valid email is required: ex@abc.xyz">
	          <span class >last name</span>
						<input class="input101" type="text" name="last_name"required="required">
					</div>
					<div class="wrap-input100 validate-input" required="required" data-validate = "Valid email is required: ex@abc.xyz">
   <span class >email</span>
						<input class="input101" type="text" name="email"required="required">
					</div>
					<div class="wrap-input100 validate-input" required="required" data-validate = "Valid email is required: ex@abc.xyz">
						<span class >re enetr email</span>

						<input class="input101" type="text" name="remail"required="required">
					</div>
					<div class="wrap-input100 validate-input" required="required" data-validate = "Valid email is required: ex@abc.xyz">
<span class >password</span>
						<input class="input101" type="password" name="password"required="required">
					</div>
					<div class="wrap-input100 validate-input">

						<span class >gender</span>&nbsp&nbsp&nbsp
						<select name="sex" style="width:120;height:35;font-size:18px;padding:3;" required="required">
									<option value="Select Sex:"> Select Sex: </option>
									<option value="Female"> Female </option>
									<option value="Male"> Male </option>
								</select>

					</div>
					<div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
				<span class >dob</span>
				<select name="month" style="width:80;font-size:18px;height:32;padding:3;" required="required">
<option value="Month:"> Month: </option>

<script type="text/javascript">

	var m=new Array("","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
	for(i=1;i<=m.length-1;i++)
	{
		document.write("<option value='"+i+"'>" + m[i] + "</option>");
	}
</script>

</select><select name="day" style="width:63;font-size:18px;height:32;padding:3;" required="required">
	<option value="Day:"> Day: </option>

	<script type="text/javascript">

		for(i=1;i<=31;i++)
		{
			document.write("<option value='"+i+"'>" + i + "</option>");
		}

	</script>

	</select><select name="year" style="width:70; font-size:18px; height:32; padding:3;">
	<option value="Year:"> Year: </option>

	<script type="text/javascript">

		for(i=1996;i>=1960;i--)
		{
			document.write("<option value='"+i+"'>" + i + "</option>");
		}

	</script>

</select>					</div>
<input type="hidden" name="fb_join_time">
		<div class="container-login100-form-btn">  <input class="login100-form-btn" type="submit" name="signup" value="Sign Up" id="sign_button" / onClick="time_get()"> </div>
		</form>



</div>
</div>
</div>

	</form>



<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->

<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>
